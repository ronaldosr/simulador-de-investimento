package br.com.ronaldosr.simuladorinvestimento.service;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.repository.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public List<Investimento> listarTiposInvestimento() {
        return investimentoRepository.findAll();
    }

    public Investimento criarNovoTipoInvestimento(Investimento novoTipoInvestimento) {
        try {
            return investimentoRepository.save(novoTipoInvestimento);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao inserir o investimento: "
                    + e.getCause().getCause().getMessage(), e);
        }
    }

}
