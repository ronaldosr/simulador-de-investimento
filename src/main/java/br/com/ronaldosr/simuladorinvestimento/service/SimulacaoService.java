package br.com.ronaldosr.simuladorinvestimento.service;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investidor;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.repository.InvestidorRepository;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.repository.InvestimentoRepository;
import br.com.ronaldosr.simuladorinvestimento.service.domain.Simulacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestidorRepository investidorRepository;

    public List<Investidor> listarSimulacoesInvestimento() {
        return investidorRepository.findAll();
    }

    public Simulacao simularInvestimento(Investidor investidor, int idInvestimento) {

        Optional<Investimento> investimento = investimentoRepository.findById(idInvestimento);

        if (!investimento.isPresent()) {
            throw new RuntimeException("Investimento não cadastrado.");
        } else {
            Simulacao simulacao = new Simulacao();
            simulacao = this.inserirInvestimento(investidor, investimento.get());
            return simulacao;
        }
    }

    private Simulacao inserirInvestimento(Investidor investidor, Investimento investimento) {
        Simulacao simulacao = new Simulacao();
        try {
            investidor.adicionarInvestimento(investimento);
            investidorRepository.save(investidor);

            simulacao.adicionarSimulacao(investidor.getNome(), investidor.getEmail(), investidor.getValorAplicado(),
                    investidor.getQuantidadeMeses());

            double montante = this.calcularMontante(
                    investidor.getValorAplicado(), investimento.getTaxaJurosMensal(), investidor.getQuantidadeMeses());

            double rendimentoMedioPeriodo = this.calcularRendimentoMedio(
                    investidor.getValorAplicado(), montante, investidor.getQuantidadeMeses());

            simulacao.setRendimentoMedioPeriodo(rendimentoMedioPeriodo);
            simulacao.setMontante(montante);

        } catch (DataAccessException e) {
            throw new RuntimeException("Não foi possível armazenar os dados do investidor. Exception: " +
                    e.getRootCause(), e);
        }
        return simulacao;
    }

    private double calcularMontante(double valorAplicado, double taxaJurosMensal, int quantidadeMeses) {
        return valorAplicado * Math.pow((1 + (taxaJurosMensal / 100)), quantidadeMeses);
    }

    private double calcularRendimentoMedio(double valorAplicado, double montante, int quantidadeMeses) {
        return (montante - valorAplicado) / quantidadeMeses;
    }
}
