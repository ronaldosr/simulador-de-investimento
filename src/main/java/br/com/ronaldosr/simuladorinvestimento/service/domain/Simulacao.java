package br.com.ronaldosr.simuladorinvestimento.service.domain;

public class Simulacao {

    private String nomeInvestidor;
    private String email;
    private double valorAplicado;
    private int quantidadeMeses;
    private int idInvestimento;
    private double montante;
    private double rendimentoMedioPeriodo;

    public Simulacao() {
    }

    public String getNomeInvestidor() {
        return nomeInvestidor;
    }

    public void setNomeInvestidor(String nomeInvestidor) {
        this.nomeInvestidor = nomeInvestidor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public double getRendimentoMedioPeriodo() {
        return rendimentoMedioPeriodo;
    }

    public void setRendimentoMedioPeriodo(double rendimentoMedioPeriodo) {
        this.rendimentoMedioPeriodo = rendimentoMedioPeriodo;
    }

    @Override
    public String toString() {
        return "Simulacao{" +
                "nomeInvestidor='" + nomeInvestidor + '\'' +
                ", email='" + email + '\'' +
                ", valorAplicado=" + valorAplicado +
                ", quantidadeMeses=" + quantidadeMeses +
                ", idInvestimento=" + idInvestimento +
                ", montante=" + montante +
                ", rendimentoMedioPeriodo=" + rendimentoMedioPeriodo +
                '}';
    }
    
    public void adicionarSimulacao(String nome, String email, double valorAplicado, int quantidadeMeses) {
        this.nomeInvestidor = nome;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
    }
}
