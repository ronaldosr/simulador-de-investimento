package br.com.ronaldosr.simuladorinvestimento.endpoint.controller;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import br.com.ronaldosr.simuladorinvestimento.endpoint.request.SimulacaoRequest;
import br.com.ronaldosr.simuladorinvestimento.endpoint.request.InvestimentoRequest;
import br.com.ronaldosr.simuladorinvestimento.endpoint.response.ResumoSimulacaoResponse;
import br.com.ronaldosr.simuladorinvestimento.endpoint.response.InvestimentoResponse;
import br.com.ronaldosr.simuladorinvestimento.service.SimulacaoService;
import br.com.ronaldosr.simuladorinvestimento.service.InvestimentoService;
import br.com.ronaldosr.simuladorinvestimento.service.domain.Simulacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public List<InvestimentoResponse> listarTiposInvestimento () {
        InvestimentoResponse investimentoResponse = new InvestimentoResponse();
        return investimentoResponse.converterParaListaTiposInvestimentoResponse(
                investimentoService.listarTiposInvestimento()
        );
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public InvestimentoResponse criarNovoTipoInvestimento(@RequestBody @Valid InvestimentoRequest investimentoRequest) {
        InvestimentoResponse investimentoResponse = new InvestimentoResponse();
        try {
            Investimento investimento = investimentoService.criarNovoTipoInvestimento(
                            investimentoRequest.converterParaTipoInvestimento(investimentoRequest)
            );
            return investimentoResponse.converterParaTipoInvestimentoResponse(investimento);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public ResumoSimulacaoResponse simularInvestimento(@RequestBody @Valid SimulacaoRequest simulacaoRequest, @PathVariable(name = "id") int id) {
        ResumoSimulacaoResponse resumoSimulacaoResponse = new ResumoSimulacaoResponse();

        try {
            Simulacao simulacao = simulacaoService.simularInvestimento(
                    simulacaoRequest.converterParaInvestidor(simulacaoRequest), id);
            return resumoSimulacaoResponse.converterParaResumoSimulacaoResponse(simulacao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }


    }

}
