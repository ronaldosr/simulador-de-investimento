package br.com.ronaldosr.simuladorinvestimento.endpoint.response;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import br.com.ronaldosr.simuladorinvestimento.utilitario.Util;

import java.util.ArrayList;
import java.util.List;

public class InvestimentoResponse {

    private int id;
    private String nome;
    private double rendimentoAoMes;

    public InvestimentoResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public InvestimentoResponse converterParaTipoInvestimentoResponse(Investimento tipoInvestimento) {
        InvestimentoResponse investimentoResponse = new InvestimentoResponse();
        investimentoResponse.setId(tipoInvestimento.getId());
        investimentoResponse.setNome(tipoInvestimento.getNome());
        investimentoResponse.setRendimentoAoMes(Util.formatarDouble(tipoInvestimento.getTaxaJurosMensal()));
        return investimentoResponse;
    }

    public List<InvestimentoResponse> converterParaListaTiposInvestimentoResponse(List<Investimento> tiposInvestimento) {
        List<InvestimentoResponse> tiposInvestimentoResponse = new ArrayList<>();
        for (Investimento investimento : tiposInvestimento) {
            InvestimentoResponse investimentoResponse = new InvestimentoResponse();
            investimentoResponse.setId(investimento.getId());
            investimentoResponse.setNome(investimento.getNome());
            investimentoResponse.setRendimentoAoMes(Util.formatarDouble(investimento.getTaxaJurosMensal()));
            tiposInvestimentoResponse.add(investimentoResponse);
        }
        return tiposInvestimentoResponse;
    }
}
