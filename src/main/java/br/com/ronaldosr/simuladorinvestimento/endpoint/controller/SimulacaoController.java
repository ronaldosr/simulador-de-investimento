package br.com.ronaldosr.simuladorinvestimento.endpoint.controller;

import br.com.ronaldosr.simuladorinvestimento.endpoint.response.SimulacaoResponse;
import br.com.ronaldosr.simuladorinvestimento.service.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @GetMapping
    public List<SimulacaoResponse> listarSimulacoesInvestimento() {
        SimulacaoResponse simulacaoResponse = new SimulacaoResponse();
        return simulacaoResponse.converterParaSimulacaoResponse(simulacaoService.listarSimulacoesInvestimento());
    }

}
