package br.com.ronaldosr.simuladorinvestimento.endpoint.request;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;

import javax.validation.constraints.*;

public class InvestimentoRequest {

    @NotEmpty(message = "Por gentileza, informe um nome.")
    @Size(min = 3, max = 50, message = "O nome deve conter entre 3 e 50 caracteres.")
    private String nome;

    @NotNull(message = "Por gentileza, informe a taxa de juros ao mês.")
    @Digits(integer = 3, fraction = 2, message = "Por gentileza, informe uma taxa de juros válida.")
    @DecimalMax(value = "100.00", message = "A taxa máxima é de 100,00%.")
    @DecimalMin(value = "0.01", message = "A taxa mínima é de 0,01%.")
    private double taxaJurosMensal;

    public InvestimentoRequest() {
    }

    public String getNome() {
        return nome;
    }

    public double getTaxaJurosMensal() {
        return taxaJurosMensal;
    }
    public Investimento converterParaTipoInvestimento(InvestimentoRequest investimentoRequest) {
        Investimento novoTipoInvestimento = new Investimento();
        novoTipoInvestimento.setNome(investimentoRequest.getNome());
        novoTipoInvestimento.setTaxaJurosMensal(investimentoRequest.getTaxaJurosMensal());
        return novoTipoInvestimento;
    }
}
