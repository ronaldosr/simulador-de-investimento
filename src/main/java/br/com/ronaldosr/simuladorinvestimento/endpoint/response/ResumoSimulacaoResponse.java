package br.com.ronaldosr.simuladorinvestimento.endpoint.response;

import br.com.ronaldosr.simuladorinvestimento.service.domain.Simulacao;
import br.com.ronaldosr.simuladorinvestimento.utilitario.Util;

public class ResumoSimulacaoResponse {

    private double montante;
    private int periodoEmMeses;
    private double rendimentoMedioPeriodo;


    public ResumoSimulacaoResponse() {
    }

    public double getRendimentoMedioPeriodo() {
        return rendimentoMedioPeriodo;
    }

    public void setRendimentoMedioPeriodo(double rendimentoMedioPeriodo) {
        this.rendimentoMedioPeriodo = rendimentoMedioPeriodo;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public int getPeriodoEmMeses() {
        return periodoEmMeses;
    }

    public void setPeriodoEmMeses(int periodoEmMeses) {
        this.periodoEmMeses = periodoEmMeses;
    }

    public ResumoSimulacaoResponse converterParaResumoSimulacaoResponse(Simulacao simulacao) {
        ResumoSimulacaoResponse resumoSimulacaoResponse = new ResumoSimulacaoResponse();
        resumoSimulacaoResponse.setMontante(Util.formatarDouble(simulacao.getMontante()));
        resumoSimulacaoResponse.setRendimentoMedioPeriodo(Util.formatarDouble(simulacao.getRendimentoMedioPeriodo()));
        resumoSimulacaoResponse.setPeriodoEmMeses(simulacao.getQuantidadeMeses());
        return resumoSimulacaoResponse;
    }
}
