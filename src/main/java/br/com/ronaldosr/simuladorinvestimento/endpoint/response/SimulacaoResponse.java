package br.com.ronaldosr.simuladorinvestimento.endpoint.response;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investidor;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import br.com.ronaldosr.simuladorinvestimento.utilitario.Util;

import java.util.ArrayList;
import java.util.List;

public class SimulacaoResponse {

    private String nomeInvestidor;
    private String email;
    private double valorAplicado;
    private int quantidadeMeses;
    private int idInvestimento;

    public SimulacaoResponse() {
    }

    public String getNomeInvestidor() {
        return nomeInvestidor;
    }

    public void setNomeInvestidor(String nomeInvestidor) {
        this.nomeInvestidor = nomeInvestidor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public List<SimulacaoResponse> converterParaSimulacaoResponse(List<Investidor> listaSimulacoesInvestimento) {
        List<SimulacaoResponse> simulacoesResponse = new ArrayList<>();
        for (Investidor investidor : listaSimulacoesInvestimento) {
            for (Investimento investimento : investidor.getInvestimentos()) {
                SimulacaoResponse simulacaoResponse = new SimulacaoResponse();
                simulacaoResponse.setNomeInvestidor(investidor.getNome());
                simulacaoResponse.setEmail(investidor.getEmail());
                simulacaoResponse.setValorAplicado(Util.formatarDouble(investidor.getValorAplicado()));
                simulacaoResponse.setQuantidadeMeses(investidor.getQuantidadeMeses());
                simulacaoResponse.setIdInvestimento(investimento.getId());
                simulacoesResponse.add(simulacaoResponse);
            }
        }
        return simulacoesResponse;
    }
}
