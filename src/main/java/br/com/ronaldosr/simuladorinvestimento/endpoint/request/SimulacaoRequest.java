package br.com.ronaldosr.simuladorinvestimento.endpoint.request;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investidor;
import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

public class SimulacaoRequest {

    @NotEmpty(message = "Por gentileza, informe um nome.")
    @Size(min = 3, max = 80, message = "O nome deve conter entre 3 e 80 caracteres.")
    private String nome;

    @Email(message = "Por gentileza, informe um e-mail válido.")
    @Size(min = 10, max = 50, message = "O email deve conter entre 10 e 50 caracteres.")
    private String email;

    @NotNull(message = "Por gentileza, informe o valor a ser aplicado ao mês.")
    @Digits(integer = 10, fraction = 2, message = "Por gentileza, informe um valor de aplicação válido.")
    @DecimalMin(value = "100.00", message = "O investimento mínimo é de R$ 100,00.")
    private double valorAplicado;

    @NotNull(message = "Por gentileza, informe a quantidade de meses para aplicação.")
    @Min(value = 1, message = "O período mínimo de investimento é um mês.")
    private int quantidadeMeses;

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public Investidor converterParaInvestidor(SimulacaoRequest simulacaoRequest) {
        Investidor investidor = new Investidor();
        investidor.setNome(simulacaoRequest.getNome());
        investidor.setEmail(simulacaoRequest.getEmail());
        investidor.setValorAplicado(simulacaoRequest.getValorAplicado());
        investidor.setQuantidadeMeses(simulacaoRequest.getQuantidadeMeses());

        List<Investimento> investimentos = new ArrayList<>();
        investidor.setInvestimentos(investimentos);

        return investidor;
    }
}
