package br.com.ronaldosr.simuladorinvestimento.utilitario;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

public class Util {

    public static double formatarDouble(double x) {
        DecimalFormat df = new DecimalFormat(".##");
        return Double.valueOf(df.format(x).replace(',', '.'));
    }
}
