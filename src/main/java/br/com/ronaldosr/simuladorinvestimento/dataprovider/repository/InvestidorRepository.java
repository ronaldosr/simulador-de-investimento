package br.com.ronaldosr.simuladorinvestimento.dataprovider.repository;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investidor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvestidorRepository extends JpaRepository<Investidor, Integer> {
}
