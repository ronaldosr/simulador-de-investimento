package br.com.ronaldosr.simuladorinvestimento.dataprovider.repository;

import br.com.ronaldosr.simuladorinvestimento.dataprovider.data.Investimento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvestimentoRepository extends JpaRepository<Investimento, Integer> {
}
