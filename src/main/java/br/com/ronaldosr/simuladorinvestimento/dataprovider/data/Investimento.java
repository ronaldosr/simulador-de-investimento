package br.com.ronaldosr.simuladorinvestimento.dataprovider.data;

import javax.persistence.*;

@Entity
@Table(name = "investimento")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome", length = 50, unique = true, nullable = false)
    private String nome;

    @Column(name = "taxaJurosMes", nullable = false)
    private double taxaJurosMensal;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTaxaJurosMensal() {
        return taxaJurosMensal;
    }

    public void setTaxaJurosMensal(double taxaJurosMensal) {
        this.taxaJurosMensal = taxaJurosMensal;
    }

    @Override
    public String toString() {
        return "Investimento{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", taxaJuros=" + taxaJurosMensal +
                '}';
    }
}
