package br.com.ronaldosr.simuladorinvestimento.dataprovider.data;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "investidor")
public class Investidor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome", length = 80, nullable = false)
    private String nome;

    @Column(name = "email", length = 50, nullable = false)
    private String email;

    @Column(name = "valorAplicado", nullable = false)
    private double valorAplicado;

    @Column(name = "quantidadeMeses", nullable = false)
    private int quantidadeMeses;

    @ManyToMany
    private List<Investimento> investimentos;

    public Investidor() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public List<Investimento> getInvestimentos() {
        return investimentos;
    }

    public void setInvestimentos(List<Investimento> investimentos) {
        this.investimentos = investimentos;
    }

    @Override
    public String toString() {
        return "Investidor{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", valorAplicado=" + valorAplicado +
                ", quantidadeMeses=" + quantidadeMeses +
                ", investimentos=" + investimentos +
                '}';
    }

    public void adicionarInvestimento(Investimento investimento) {
        this.investimentos.add(investimento);
    }
}
